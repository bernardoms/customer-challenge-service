# Customer Challenge

## Technologies used
* JAVA 11
* Spring boot 2.5.0 (Just for IOC)
* Maven
* Junit 5
* Docker

## How to Run

 - With Docker: 
 `./mvnw clean package` 
 `docker build -t customer --output dockeroutput .`
  The output will be in the dockeroutput folder on the project dir.
 
 - Without Docker:
  You are going to need java 11.
  `./mvnw clean package`
  `java -jar target/customer-service-0.0.1-SNAPSHOT.jar`
  
There are tests, and the tests are in the test folder of the project. When you run ./mvnw clean package to create the jar
its runs all the tests. 
