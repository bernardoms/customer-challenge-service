package com.bernardoms.customerservice.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.bernardoms.customerservice.model.Customer;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Collections;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;

@ExtendWith(MockitoExtension.class)
class FileParserServiceTest {
  @InjectMocks
  private FilerParserService filerParserService;

  @Mock
  private ResourceLoader resourceLoader;

  @Mock
  private ObjectMapper objectMapper;

  @Test
  void shouldReturnParsedCustomers() throws IOException {
    var resource = mock(Resource.class);
    var customer = "{\"latitude\": \"52.986375\", \"user_id\": 12, \"name\": \"Christina McArdle\", \"longitude\": \"-6.043701\"}";

    var customerObject = new Customer();
    customerObject.setLatitude(52.986375);
    customerObject.setLongitude(-6.043701);
    customerObject.setName("Christina McArdle");
    customerObject.setUserId(12L);

    var stream = new ByteArrayInputStream(customer.getBytes(StandardCharsets.UTF_8));
    when(resourceLoader.getResource(anyString())).thenReturn(resource);
    when(resource.getInputStream()).thenReturn(stream);
    when(objectMapper.readValue(customer, Customer.class)).thenReturn(customerObject);

    var customers = filerParserService.parseInFile(Customer.class, "test.txt");

    assertFalse(customers.isEmpty());

    assertEquals(52.986375, customers.get(0).getLatitude());
    assertEquals(-6.043701, customers.get(0).getLongitude());
    assertEquals("Christina McArdle", customers.get(0).getName());
    assertEquals(12L, customers.get(0).getUserId());
  }

  @Test
  void shouldWriteToFileParsed() throws IOException {
    var customerObject = new Customer();
    customerObject.setLatitude(52.986375);
    customerObject.setLongitude(-6.043701);
    customerObject.setName("Christina McArdle");
    customerObject.setUserId(12L);

    var customers = Collections.singletonList(customerObject);

    filerParserService.parseOutFile(customers, "");

    verify(objectMapper, times(1)).writeValue(any(File.class),eq(customers));
  }
}
