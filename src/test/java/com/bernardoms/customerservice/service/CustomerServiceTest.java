package com.bernardoms.customerservice.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.bernardoms.customerservice.config.CustomerConfig;
import com.bernardoms.customerservice.helper.DistanceCalculatorHelper;
import com.bernardoms.customerservice.model.Customer;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class CustomerServiceTest {

  @Mock
  private FilerParserService filerParserService;

  @Mock
  private DistanceCalculatorHelper distanceCalculatorHelper;

  @Mock
  private CustomerConfig customerConfig;

  @Captor
  private ArgumentCaptor<List<Customer>> customerFiltered;

  @InjectMocks
  private CustomerService customerService;

  @Test
  void shouldFilterCustomerByOfficeDistanceAndWriteToFile() throws IOException {
    var customerWithDistanceOfficeLimit = new Customer();
    customerWithDistanceOfficeLimit.setName("test test");
    customerWithDistanceOfficeLimit.setUserId(12L);
    customerWithDistanceOfficeLimit.setLatitude(50.000);
    customerWithDistanceOfficeLimit.setLongitude(-5.000);

    var customerWithoutDistanceOfficeLimit = new Customer();
    customerWithoutDistanceOfficeLimit.setName("test2 test2");
    customerWithoutDistanceOfficeLimit.setUserId(1L);
    customerWithoutDistanceOfficeLimit.setLatitude(30.000);
    customerWithoutDistanceOfficeLimit.setLongitude(-2.000);

    var customerWithDistanceOfficeLimit2 = new Customer();
    customerWithDistanceOfficeLimit2.setName("test3 test3");
    customerWithDistanceOfficeLimit2.setUserId(4L);
    customerWithDistanceOfficeLimit2.setLatitude(10.000);
    customerWithDistanceOfficeLimit2.setLongitude(-2.000);

    var customerWithDistanceOfficeLimit3 = new Customer();
    customerWithDistanceOfficeLimit3.setName("test4 test4");
    customerWithDistanceOfficeLimit3.setUserId(2L);
    customerWithDistanceOfficeLimit3.setLatitude(40.000);
    customerWithDistanceOfficeLimit3.setLongitude(-2.000);

    when(customerConfig.getOfficeDistanceLatitude()).thenReturn(20.000);
    when(customerConfig.getOfficeDistanceLongitude()).thenReturn(-5.000);
    when(customerConfig.getOfficeDistanceLimit()).thenReturn(100.00);
    when(customerConfig.getInputFile()).thenReturn("test.txt");
    when(customerConfig.getOutputFile()).thenReturn("test-output.txt");

    when(distanceCalculatorHelper.calculateDistance(50.000, 20.000, -5.000, -5.000)).thenReturn(100.00);
    when(distanceCalculatorHelper.calculateDistance(30.000, 20.000, -2.000, -5.000)).thenReturn(101.00);
    when(distanceCalculatorHelper.calculateDistance(10.000, 20.000, -2.000, -5.000)).thenReturn(99.00);
    when(distanceCalculatorHelper.calculateDistance(40.000, 20.000, -2.000, -5.000)).thenReturn(00.10);

    when(filerParserService.parseInFile(Customer.class, "test.txt")).thenReturn(Arrays
        .asList(customerWithDistanceOfficeLimit, customerWithDistanceOfficeLimit2, customerWithoutDistanceOfficeLimit,
            customerWithDistanceOfficeLimit3));

    customerService.getCustomerLimitedDistanceFromOffice();

    verify(filerParserService, times(1)).parseOutFile(customerFiltered.capture(), eq("test-output.txt"));

    assertFalse(customerFiltered.getValue().isEmpty());
    assertEquals(3, customerFiltered.getValue().size());
    assertEquals(2L, customerFiltered.getValue().get(0).getUserId());
    assertEquals(4L, customerFiltered.getValue().get(1).getUserId());
    assertEquals(12L, customerFiltered.getValue().get(2).getUserId());
  }
}
