package com.bernardoms.customerservice.helper;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

class DistanceCalculatorHelperTest {

  @Test
  void shouldCalculateDistanceUsingHarvesineFormula() {
    var distanceCalculatorHelper = new DistanceCalculatorHelper();
    assertEquals(10.566936288868613, distanceCalculatorHelper.calculateDistance(53.2451022, 53.339428, -6.238335, -6.257664));
  }
}
