package com.bernardoms.customerservice.helper;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class DistanceCalculatorHelper {

  public double calculateDistance(double lat1,
      double lat2, double lon1,
      double lon2) {

    lon1 = Math.toRadians(lon1);
    lon2 = Math.toRadians(lon2);
    lat1 = Math.toRadians(lat1);
    lat2 = Math.toRadians(lat2);

    // Haversine formula https://en.wikipedia.org/wiki/Great-circle_distance
    var dlon = lon2 - lon1;
    var dlat = lat2 - lat1;
    var a = Math.pow(Math.sin(dlat / 2), 2)
        + Math.cos(lat1) * Math.cos(lat2)
        * Math.pow(Math.sin(dlon / 2), 2);

    var c = 2 * Math.asin(Math.sqrt(a));

    // Radius of earth in kilometers.
    var r = 6371;

    // calculate the result
    return (c * r);
  }
}
