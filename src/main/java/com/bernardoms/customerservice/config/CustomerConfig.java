package com.bernardoms.customerservice.config;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "customer")
@Getter
@Setter
public class CustomerConfig {

  private double officeDistanceLimit;
  private String outputFile;
  private double officeDistanceLatitude;
  private double officeDistanceLongitude;
  private String inputFile;

  @Bean
  public ObjectMapper getObjectMapper() {
    return new ObjectMapper().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
  }
}
