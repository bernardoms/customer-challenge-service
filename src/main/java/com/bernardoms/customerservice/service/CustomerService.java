package com.bernardoms.customerservice.service;

import com.bernardoms.customerservice.config.CustomerConfig;
import com.bernardoms.customerservice.helper.DistanceCalculatorHelper;
import com.bernardoms.customerservice.model.Customer;
import java.io.IOException;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@Log4j2
public class CustomerService {

  private final FilerParserService filerParserService;

  private final DistanceCalculatorHelper distanceCalculatorHelper;

  private final CustomerConfig customerConfig;

  public void getCustomerLimitedDistanceFromOffice() throws IOException {
    List<Customer> customers = filerParserService.parseInFile(Customer.class, customerConfig.getInputFile());

    customers
        .forEach(c -> c.setDistance(
            distanceCalculatorHelper
                .calculateDistance(c.getLatitude(), customerConfig.getOfficeDistanceLatitude(), c.getLongitude(),
                    customerConfig.getOfficeDistanceLongitude())));

    List<Customer> customersFilteredByOfficeDistance = customers.stream()
        .filter(c -> c.getDistance() <= customerConfig.getOfficeDistanceLimit())
        .sorted(Comparator.comparing(Customer::getUserId)).collect(
            Collectors.toList());

    log.info("Found {} with {} km distance to office", customersFilteredByOfficeDistance.size(), customerConfig.getOfficeDistanceLimit());
    filerParserService.parseOutFile(customersFilteredByOfficeDistance, customerConfig.getOutputFile());
  }
}
