package com.bernardoms.customerservice.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Service;

@RequiredArgsConstructor
@Service
@Log4j2
public class FilerParserService {
  private final ResourceLoader resourceLoader;
  private final ObjectMapper objectMapper;

  public <T> List<T> parseInFile(Class<T> model, String inputFile) throws IOException {
    var resource = resourceLoader.getResource("classpath:/" + inputFile);

    List<T> listObject = new ArrayList<>();
    try (BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(resource.getInputStream()))) {

      String line;
      while ((line = bufferedReader.readLine()) != null) {
        var obj = objectMapper.readValue(line, model);
        listObject.add(obj);
      }
    } catch (IOException exception) {
      log.error("Error reading the file ", exception);
      throw new IOException();
    }
    return listObject;
  }

  public <T> void parseOutFile(List<T> objects, String outputFile) throws IOException {
    var file = new File(outputFile);
    objectMapper.writeValue(file, objects);
  }
}
