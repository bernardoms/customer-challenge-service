package com.bernardoms.customerservice;

import com.bernardoms.customerservice.service.CustomerService;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CustomerServiceApplication implements ApplicationRunner {

  private final CustomerService customerService;

  public CustomerServiceApplication(CustomerService customerService) {
    this.customerService = customerService;
  }

  public static void main(String[] args) {
    SpringApplication.run(CustomerServiceApplication.class, args);
  }

  @Override
  public void run(ApplicationArguments args) throws Exception {
    customerService.getCustomerLimitedDistanceFromOffice();
  }
}
