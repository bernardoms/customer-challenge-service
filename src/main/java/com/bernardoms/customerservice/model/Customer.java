package com.bernardoms.customerservice.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class Customer {
  private String name;
  @JsonProperty("user_id")
  private Long userId;
  @JsonIgnore
  private Double distance;
  private Double latitude;
  private Double longitude;
}
