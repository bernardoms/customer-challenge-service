FROM openjdk:11 AS generate-output
WORKDIR app

ADD target/*.jar /app/customer.jar

RUN java -jar customer.jar

FROM scratch AS export-stage
COPY --from=generate-output /app/output.txt .
